

[2012.04.03 Mike Chung]
* copied this package from "ros-uw/gambit" stack in "robot@chess-laptop.dyn.cs.washington.edu".
* seems like another a wrapper, or library only type package for TRAJECTORY SMOOTHING purposes.
  ** same clues as coplay, dynamixel, safetyboard
* compiles fine on Ubuntu 11.10.
