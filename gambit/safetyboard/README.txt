

[2012.04.03 Mike Chung]
* copied this package from "ros-uw/gambit" stack in "robot@chess-laptop.dyn.cs.washington.edu".
* seems like a HIGH-LEVEL COMMUNICATION wrapper, or library only type package.
  ** clue1: only compiles libraries, no executable at all! - see "CMakeLists.txt"
  ** clue2: same as the dynamixel package.
* compiles fine on Ubuntu 11.10., if the dynamixel package compiles okay. (dependent on that package.)
