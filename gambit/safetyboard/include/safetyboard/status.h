#ifndef __safetyboard_status_h_
#define __safetyboard_status_h_

#define STATE_RESET     0
#define STATE_IDLE      1
#define STATE_ENABLED   2
#define STATE_ACTIVE    3
#define STATE_ESTOPPED  4

#define FAULT_NONE      0
#define FAULT_ESTOP     1
#define FAULT_HEARTBEAT 2

#endif // __safetyboard_status_h_

