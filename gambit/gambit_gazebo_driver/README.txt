

[2012.02.18]

  IDEAS FOR IMPROVEMENTS

  GAZEBO - SIMULATION
    1. gambit.cpp or arm.cpp - write a get_pos() style code that blocks process until it gets new data.
    2. play little bit with mass / inertia parameters (just try very small values) to find "okay" controller

    [GOAL0]  STACK LEGO BUILDINGS!
    [GOAL1]  be able to GRASP something
    [GOAL2]  See how can I make a "STABLE" controller using current PID and PHYSICS PARAMETER SETTINGS
	[GOAL3]  be able to USE SAME MANIP CODE

    * SOFTWARE FIX := experiment with "arm_interface.h/cpp" code.
    * HARDWARE FIX := try to get correct inertia / damping values. (NOT %100 POSSIBLE)
